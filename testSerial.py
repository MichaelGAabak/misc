import sys
import glob
import serial
import time
import array

def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


if __name__ == '__main__':

	ports = serial_ports()
	print(ports)
	for port in ports:
        initialise(port)

def send_data(port, to_send):
	ser = serial.Serial(port, 115200, serial.EIGHTBITS, serial.PARITY_NONE, serial.STOPBITS_ONE)
	ser.write(tosend)
    #,'\r\n'
	time.sleep(0.1)
	ser.close()

def initialise(port):
    to_send = bitarray(11111110) #byte 1, beginning flag
    to_send.append(11110101) #byte 2 Instruction Type
    to_send.append(00000001) #byte 3 Status
    send_data(port, to_send)

def setup(port):
    to_send = bitarray(11111110)
    to_send.append(11110111)
    to_send.append(11001100)
    to_send.append(11001100)
    to_send.append(11001100)
    to_send.append(11001100)
    to_send.append(11001100)
    to_send.append(11001100)
    send_data(port, to_send)

def move(port):
    to_send = bitarray(11111110)
    to_send.append(11111001)
    to_send.append(00000111)
    to_send.append(01111111)
    to_send.append(00000111)
    to_send.append(01111111)
    to_send.append(00000111)
    to_send.append(01111111)
    to_send.append(00000111)
    to_send.append(01111111)
    to_send.append(00000111)
    to_send.append(01111111)
    to_send.append(00000111)
    to_send.append(01111111)
    send_data(port, to_send)
